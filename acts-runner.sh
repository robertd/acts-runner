#!/bin/sh

# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

set -e


if ! test $# -eq 1 || test ! -d "$1"; then
    echo "Usage: acts-runner <testcases-directory>" 1>&2
    exit 22
fi

REQUIRED_COMMANDS="run-acts-test.sh"
for CMD in $REQUIRED_COMMANDS; do
    if ! command -v "$CMD" > /dev/null; then
        echo "Tool: $CMD not found!"
        exit 1
    fi
done

TCDIR=$1
TESTS_CNT=0
FAILED_TESTS=0
for TESTCASE_JSON in "$TCDIR"/*.json; do
    TESTS_CNT=$((TESTS_CNT + 1))
    if ! run-acts-test.sh "$TESTCASE_JSON"; then
        FAILED_TESTS=$((FAILED_TESTS + 1))
    fi
done

if test "$FAILED_TESTS" -ne 0; then
    echo "SUMMARY: FAIL - $FAILED_TESTS out of total $TESTS_CNT test suites have failed!"
    exit 1
fi

echo "SUMMARY: PASS - all $TESTS_CNT test suites have passed!"
