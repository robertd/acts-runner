<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# acts-runner

This project contains scripts for executing acts test suite on the target device.

NOTE: Scripts are tested on Ubuntu 20.04.

## Prerequisites

To use acts-runner scripts following tools have to be installed on the host system:

- `hdc` - OpenHarmony device connector
- `jq` - commandline JSON processor
- `unzip`

## Usage

There are three scripts available, each of which can be used as a stand-alone tool:

- `acts-runner.sh`
- `run-acts-test.sh`
- `run-hap-test.sh`

NOTE: All scripts should be made available in your shell environment (e.g. this directory added to the PATH variable).

Before any of the provided scripts can be successfully used, first the target device have to be connected and available in `hdc` tool, for the USB connected device:

```sh
$ hdc list targets
150100384754463452061bc04d5267b7
```
and for TCP/IP connected device(using `hdc tconn 192.168.7.2:42407` command):
```sh
$ hdc list targets
192.168.7.2:42407
```

### `acts-runner.sh`

This script allows executing entire ACTS test suite and gather a complete report.

NOTE: Report is printed to `stdout` while all the diagnostic information to `stderr`, therefore for gathering report one should redirect the `stdout` to a file, like so:

```sh
$ acts-runner.sh <openharmony-source-tree>/out/ohos-arm-release/suites/acts/testcases > acts-testsuite-report.txt
```

Report has the following format:

```
Testcase ace_standard.json: PASS
total cases:16;failure 0,error 0,pass 16; consuming 16.68S
================
Testcase ActsAnsGetSlotTestCallback.json: FAIL
total cases:5;failure 4,error 0,pass 1; consuming 6.86S
================
...
```

### `run-acts-test.sh`

This script allows executing single ACTS test case described by the json file. It is used by the `acts-runner.sh` script and the output is a single test case report:

```
Testcase ace_standard.json: PASS
total cases:16;failure 0,error 0,pass 16; consuming 16.68S
================
```

There are two supported types of tests:

- JSUnitTest - installable hap packages handled by the `run-hap-test.sh` script
- CppTest - binary test to be uploaded and executed on target

Example (by default both diagnostics - stderr - and report - stdout - is printed to the console):

```
$ run-acts-test.sh ./out/ohos-arm-release/suites/acts/testcases/windowhaptest.json 
[Info]App install path:/home/robert/prj/gitee_3.0/code-v3.0-LTS/OpenHarmony/./out/ohos-arm-release/suites/acts/testcases/windowhaptest.hap, queuesize:0, msg:install bundle successfully. 
AppMod finish
core log clear success 
app log clear success 

Starting ability: "com.test.window.MainAbility" from bundle: "com.test.window"
[Info]App uninstall path:, queuesize:0, msg:uninstall bundle successfully. 
AppMod finish
Testcase windowhaptest.json: PASS
total cases:18;failure 0,error 0,pass 18; consuming 8.04S
================
```

### `run-hap-test.sh`

This script allows executing single ACTS test hap package. It is used by the `run-acts-test.sh` script and the output is a one line test summary:

```
total cases:18;failure 0,error 0,pass 18; consuming 8.04S
```

Example (by default both diagnostics - stderr - and report - stdout - is printed to the console):

```
$ run-hap-test.sh ./out/ohos-arm-release/suites/acts/testcases/windowhaptest.hap 
[Info]App install path:/home/robert/prj/gitee_3.0/code-v3.0-LTS/OpenHarmony/./out/ohos-arm-release/suites/acts/testcases/windowhaptest.hap, queuesize:0, msg:install bundle successfully. 
AppMod finish
core log clear success 
app log clear success 

Starting ability: "com.test.window.MainAbility" from bundle: "com.test.window"
[Info]App uninstall path:, queuesize:0, msg:uninstall bundle successfully. 
AppMod finish
total cases:18;failure 0,error 0,pass 18; consuming 8.04S
```
