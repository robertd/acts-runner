#!/bin/sh

# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

set -e

print_usage() (
    echo "Usage: run-hap-test.sh [-t <timeout_in_seconds>] <hap-file>"
)

arguments_valid() {
    test $# -eq 1 && test -f "$1"
}

required_tools_available() (
    REQUIRED_TOOLS="hdc jq unzip"
    for CMD in $REQUIRED_TOOLS; do
        if ! command -v "$CMD" > /dev/null; then
            echo "Tool: $CMD not found!"
            return 1
        fi
    done
)

dut_available() (
    hdc shell echo "DUT-online" | grep -q "DUT-online"
)

temp_dir() (
    WORKDIR=$(mktemp -d -p "$(realpath .)")
    if test "$(dirname "$WORKDIR")" != "$(realpath .)"; then
        echo "ERROR: Failed to create temp directory!" 1>&2
        exit 1
    fi

    echo "$WORKDIR"
)

hap_config_json() (
    TEST_FILE=$1
    WORKDIR=$2
    unzip -q "$TEST_FILE" -d "$WORKDIR" config.json
    CONFIG_JSON="$WORKDIR/config.json"
    
    test -f "$CONFIG_JSON" && echo "$CONFIG_JSON" || exit 2
)

run_hap_test() (
    # Output of the following block will be redirected to stderr for logging
    {
        TEST_FILE=$1
        TIMEOUT_S=$2
        if test -z "$TIMEOUT_S"; then
            # Disable timeout
            TIMEOUT_S=0
        fi

        WORKDIR=$(temp_dir)
        CONFIG_JSON=$(hap_config_json "$TEST_FILE" "$WORKDIR")

        BUNDLE_NAME=$(jq -r '.app.bundleName' "$CONFIG_JSON")
        PACKAGE_NAME=$(jq -r '.module.package' "$CONFIG_JSON")
        ABILITY_NAME=$(jq -r '.module.abilities[0].name' "$CONFIG_JSON")
        if echo "$ABILITY_NAME" | grep -Eq "^\."; then
            ABILITY_NAME="$BUNDLE_NAME$ABILITY_NAME"
        fi
        rm -r "$WORKDIR"

        hdc install "$TEST_FILE"
        hdc shell hilog -r
        echo "Starting ability: \"$ABILITY_NAME\" from bundle: \"$BUNDLE_NAME\""
        HDC_AA_LOG=$(timeout "$TIMEOUT_S" hdc shell aa start -a "$ABILITY_NAME" -b "$BUNDLE_NAME")
        if ! echo "$HDC_AA_LOG" | grep -q "error"; then
            # Wait for results only if ability started successfully
            # Below hilog call should be refined with some filtering (JSApp?)
            # and not just blindly catch all the logs
            timeout "$TIMEOUT_S" hdc shell hilog | tee .TESTCACE_LOG.log | grep -Eo -m 1 "\[end\] run suites end" || true
            TESTCASE_LOG=$(cat .TESTCACE_LOG.log) || true
            rm -f .TESTCASE_LOG.log
        fi
        hdc uninstall "$PACKAGE_NAME"
    } 1>&2
    TESTCASE_REPORT=$(echo "$TESTCASE_LOG" | grep -Eo -m 1 "total cases.*") || true
    TESTCASES_PASS=$(echo "$TESTCASE_LOG" | grep -Ec "\[pass\]") || true

    # Return failure if TESTCASE_REPORT is empty
    if test -n "$TESTCASE_REPORT"; then
        echo "$TESTCASE_REPORT"
    else
        # This is temporary workaround of the fact that test suite exits upon
        # first failure without producing the summary
        if test "$TESTCASES_PASS" -gt 0; then
            echo "total cases:0;failure 1,error 0,pass $TESTCASES_PASS"
        fi
        return 1
    fi
)

TIMEOUT=0
while getopts "t:" OPT; do
    case "$OPT" in
        t)
            TIMEOUT=$OPTARG
            ;;
        ?)
            print_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))


if ! arguments_valid "$@" || ! required_tools_available; then
    print_usage
    exit 22
fi

if ! dut_available; then
    echo "DUT is unavailable - it should be listed in the following command's output:"
    echo "  $ hdc list targets"
    exit 1
fi

TEST_FILE=$1
case $(basename "$TEST_FILE") in
    *.hap)
        run_hap_test "$TEST_FILE" "$TIMEOUT"
        ;;
    *)
        print_usage
        exit 22
        ;;
esac
