#!/bin/sh

# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

set -e

arguments_valid() (
    if ! test $# -eq 1 || test ! -f "$1"; then
        return 1
    fi
    case "$1" in
        *.json)
            ;;
        *)
            return 1
            ;;
    esac
)

required_tools_available() (
    REQUIRED_TOOLS="hdc jq run-hap-test.sh"
    for CMD in $REQUIRED_TOOLS; do
        if ! command -v "$CMD" > /dev/null; then
            echo "Tool: $CMD not found!"
            return 1
        fi
    done
)

dut_available() (
    hdc shell echo "DUT-online" | grep -q "DUT-online"
)

json_element() (
    JSON_STR=$1
    KEY=$2
    if test -z "$JSON_STR"; then
        return 1
    fi
    if ! EL=$(echo "$JSON_STR" | jq -r "$KEY"); then
        return 1
    fi
    if test -n "$EL" && test "$EL" != "null"; then
        echo "$EL"
        return 0
    fi
    return 1
)

send_file() (
    FILE_TO_SEND=$1
    REMOTE_PATH=$2
    hdc file send "$FILE_TO_SEND" "$REMOTE_PATH"
)

remote_shell_command() (
    CMD=$1
    TIMEOUT_S=$2
    if test -z "$TIMEOUT_S"; then
        TIMEOUT_S=0
    fi
    timeout "$TIMEOUT_S" hdc shell "$CMD"
)

push_kit() (
    KIT=$1
    TESTCASE_DIR=$2
    PUSH_ITEMS=$(json_element "$KIT" ".push")
    PUSH_ITEM_NO=0
    # push array contains file->remote_path pairs to be send
    while PUSH_ITEM=$(json_element "$PUSH_ITEMS" ".[$PUSH_ITEM_NO]"); do
        FILE_TO_SEND=$(echo "$PUSH_ITEM" | sed 's/^\(.*\)->.*/\1/')
        REMOTE_PATH=$(echo "$PUSH_ITEM" | sed 's/^.*->\(.*\)/\1/')
        send_file "$TESTCASE_DIR/$FILE_TO_SEND" "$REMOTE_PATH"
        PUSH_ITEM_NO=$((PUSH_ITEM_NO + 1))
    done
    # post-push array contains shell commands to be executed on target
    POST_PUSH_ITEMS=$(json_element "$KIT" ".\"post-push\"")
    POST_PUSH_ITEM_NO=0
    while POST_PUSH_ITEM=$(json_element "$POST_PUSH_ITEMS" ".[$POST_PUSH_ITEM_NO]"); do
        remote_shell_command "$POST_PUSH_ITEM"
        POST_PUSH_ITEM_NO=$((POST_PUSH_ITEM_NO + 1))
    done
)

shell_kit() (
    KIT=$1
    COMMANDS=$(json_element "$KIT" ".\"run-command\"")
    # run-command array contains shell commands to be executed on target
    # and unfortunately some special commands, like remount which have
    # to be handled separately
    CMD_NO=0
    while CMD=$(json_element "$COMMANDS" ".[$CMD_NO]"); do
        case "$CMD" in
            remount)
                remote_shell_command "mount -o rw,remount /"
                ;;
            *)
                remote_shell_command "$CMD"
                ;;
        esac
        CMD_NO=$((CMD_NO + 1))
    done
)

test_prep_work() (
    # Output of the following block will be redirected to stderr for logging
    {
    TESTCASE_JSON=$1
    TESTCASE_DIR=$(dirname "$TESTCASE_JSON")

    TESTS_TO_RUN=""
    JSON_STR=$(jq -r '.' "$TESTCASE_JSON")
    if ! KITS=$(json_element "$JSON_STR" ".kits"); then
        return 1
    fi
    KIT_NO=0
    while KIT=$(json_element "$KITS" ".[$KIT_NO]"); do
        KIT_TYPE=$(json_element "$KIT" ".type")
        case "$KIT_TYPE" in
            AppInstallKit)
                TESTS_TO_RUN=$(json_element "$KIT" ".\"test-file-name\"") || return 1
                ;;
            PushKit)
                push_kit "$KIT" "$TESTCASE_DIR" || return 1
                ;;
            ShellKit)
                shell_kit "$KIT" || return 1
                ;;
            *)
                echo "Unsupported kit type: $KIT_TYPE"
                return 1
                ;;
        esac
        KIT_NO=$((KIT_NO + 1))
    done
    } 1>&2
    echo "$TESTS_TO_RUN"
)

run_cpptest() (
    TEST_DRIVER=$1
    REMOTE_TEST_PATH=$(json_element "$TEST_DRIVER" ".\"native-test-device-path\"")
    TEST_BIN=$(json_element "$TEST_DRIVER" ".\"module-name\"")
    TEST_TIMEOUT=$(json_element "$TEST_DRIVER" ".\"native-test-timeout\"")
    TEST_REPORT=$(remote_shell_command "$REMOTE_TEST_PATH/$TEST_BIN \
                    --gtest_output=xml:$REMOTE_TEST_PATH/test_details.xml" \
                    "$TEST_TIMEOUT")
    TOTAL_CNT=$(echo "$TEST_REPORT" | grep -Eo "\[==========\] Running [0-9]+ test" \
                    | sed 's/.* \([0-9]\+\) .*/\1/')
    FAIL_CNT=$(echo "$TEST_REPORT" | grep -Eo "\[  FAILED  \] [0-9]+ test" \
                    | sed 's/.* \([0-9]\+\) .*/\1/')
    PASS_CNT=$(echo "$TEST_REPORT" | grep -Eo "\[  PASSED  \] [0-9]+ test" \
                    | sed 's/.* \([0-9]\+\) .*/\1/')
    test -z "$TOTAL_CNT" && TOTAL_CNT=0
    test -z "$FAIL_CNT" && FAIL_CNT=0
    test -z "$PASS_CNT" && PASS_CNT=0
    ERROR_CNT=0
    if test "$TOTAL_CNT" -eq 0; then
        ERROR_CNT=1
        # Print complete log to stderr
        echo "ERROR: $TEST_REPORT" 1>&2
    fi

    # Print the summary line in the same format as HAP tests to use
    # the common assess_test_results function
    echo "total cases:$TOTAL_CNT;failure $FAIL_CNT,error $ERROR_CNT,pass $PASS_CNT"
)

driver() (
    TESTCASE_JSON=$1
    jq -r '.driver' "$TESTCASE_JSON"
)

assess_test_results() (
    TEST_FILE=$(basename "$1")
    TESTCASE_REPORT=$2
    TESTCASE_RET=1
    TESTCASE_RET_STR="FAIL"
    if test -n "$TESTCASE_REPORT"; then
        if echo "$TESTCASE_REPORT" | grep -q "failure 0,error 0"; then
            TESTCASE_RET=0
            TESTCASE_RET_STR="PASS"
        fi
    else
        TESTCASE_REPORT="Test did not produce any report - a timeout or other error occurred."
    fi
    echo "Testcase $TEST_FILE: $TESTCASE_RET_STR"
    echo "$TESTCASE_REPORT"
    echo "================"
    return "$TESTCASE_RET"
)


if ! arguments_valid "$@" || ! required_tools_available; then
    echo "Usage: run-acts-test.sh <testcase-json-file>" 1>&2
    exit 22
fi

if ! dut_available; then
    echo "DUT is unavailable - it should be listed in the following command's output:"
    echo "  $ hdc list targets"
    exit 1
fi

TESTCASE_JSON=$1
TESTCASE_DIR=$(dirname "$TESTCASE_JSON")

# TESTS_TO_RUN may be empty depending on the test type
if ! TESTS_TO_RUN=$(test_prep_work "$TESTCASE_JSON"); then
    echo "Prepwork for test $TESTCASE_JSON failed!" 1>&2
fi

DRIVER=$(driver "$TESTCASE_JSON")
TEST_TYPE=$(json_element "$DRIVER" ".type")
case "$TEST_TYPE" in
    JSUnitTest)
        if test -z "$TESTS_TO_RUN"; then
            # Use assess_test_results to print a failure report
            assess_test_results "$TESTCASE_JSON" "No test haps found in json!"
            exit 1
        fi

        # Set default hap test suite timeout to 60 seconds
        TIMEOUT_S=60
        if TIMEOUT_MS=$(json_element "$DRIVER" ".\"test-timeout\""); then
            TIMEOUT_S=$((TIMEOUT_MS / 1000))
        fi

        TEST_NO=0
        FAILURES=0
        while TEST_HAP=$(json_element "$TESTS_TO_RUN" ".[$TEST_NO]"); do
            # Allow test cases to fail
            TESTCASE_REPORT=$(run-hap-test.sh -t "$TIMEOUT_S" "$TESTCASE_DIR/$TEST_HAP") || true
            if ! assess_test_results "$TESTCASE_JSON" "$TESTCASE_REPORT"; then
                FAILURES=$((FAILURES + 1))
            fi
            TEST_NO=$((TEST_NO + 1))
        done
        if test "$FAILURES" -ne 0; then
            exit 1
        fi
        ;;
    CppTest)
        TESTCASE_REPORT=$(run_cpptest "$DRIVER")
        assess_test_results "$TESTCASE_JSON" "$TESTCASE_REPORT"
        ;;
    *)
        echo "Testcase $(basename "$TESTCASE_JSON"): SKIPPED"
        echo "testcase type $TEST_TYPE unsupported!"
        echo "================"
        ;;
esac

